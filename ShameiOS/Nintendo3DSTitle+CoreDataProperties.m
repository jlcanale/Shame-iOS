//
//  Nintendo3DSTitle+CoreDataProperties.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "Nintendo3DSTitle+CoreDataProperties.h"

@implementation Nintendo3DSTitle (CoreDataProperties)

+ (NSFetchRequest<Nintendo3DSTitle *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Nintendo3DSTitle"];
}

@dynamic encTitleKey;
@dynamic name;
@dynamic region;
@dynamic scrollerLetter;
@dynamic serial;
@dynamic size;
@dynamic sortOrder;
@dynamic titleID;
@dynamic type;
@dynamic isFavorite;

@end
