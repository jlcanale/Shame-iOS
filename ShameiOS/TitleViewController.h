//
//  TitleViewController.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/16/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Nintendo3DSTitle+CoreDataClass.h"

@interface TitleViewController : UIViewController

@property (nonatomic, strong) Nintendo3DSTitle *ndsTitle;

@end
