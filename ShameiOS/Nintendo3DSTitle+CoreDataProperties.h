//
//  Nintendo3DSTitle+CoreDataProperties.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "Nintendo3DSTitle+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Nintendo3DSTitle (CoreDataProperties)

+ (NSFetchRequest<Nintendo3DSTitle *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *encTitleKey;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *region;
@property (nullable, nonatomic, copy) NSString *scrollerLetter;
@property (nullable, nonatomic, copy) NSString *serial;
@property (nonatomic) int32_t size;
@property (nonatomic) int32_t sortOrder;
@property (nullable, nonatomic, copy) NSString *titleID;
@property (nullable, nonatomic, copy) NSString *type;
@property (nonatomic) BOOL isFavorite;

@end

NS_ASSUME_NONNULL_END
