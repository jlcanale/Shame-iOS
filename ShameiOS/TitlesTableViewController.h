//
//  TitlesTableViewController.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/15/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NDSDatabaseParser.h"

@interface TitlesTableViewController : UITableViewController <NDSDatabaseParserDelegate, UISearchBarDelegate>

@property (readonly) NSPersistentContainer *persistentContainer;

@end
