//
//  SettingsTableViewController.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NDSDatabaseParser.h"


@interface SettingsTableViewController : UITableViewController <NDSDatabaseParserDelegate>

@property (readonly) NSPersistentContainer *persistentContainer;


@end
