//
//  SettingsTableViewController.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "Constants.h"
#import "NDSDatabaseParser.h"
#import "Nintendo3DSTitle+CoreDataClass.h"
#import "AppDelegate.h"
#import <M13ProgressSuite/M13ProgressViewRing.h>
#import <M13ProgressSuite/M13ProgressHUD.h>

@interface SettingsTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lastDatabaseRefreshDateLabel;
@property (nonatomic, strong) M13ProgressHUD *progressHUD;

@end

@implementation SettingsTableViewController

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    if (!_persistentContainer) {
        _persistentContainer = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer;
    }
    return _persistentContainer;
}
- (IBAction)deleteDatabaseButtonPressed:(UIButton *)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"DeleteMessage", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *continueAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Nintendo3DSTitle name]];
        NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
        
        NSError *error = nil;
        [self.persistentContainer.viewContext executeRequest:delete error:&error];
        
        if (error != nil) {
            NSLog(@"Error Deleting Database: %@", error);
        }
        
        [userDefaults setObject:nil forKey:kUserDefaultsLastDatabaseUpdate];
        [userDefaults synchronize];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.lastDatabaseRefreshDateLabel.text = @"Never";
        });
        
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:continueAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)updateDatabaseButtonPressed:(UIButton *)sender {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"WarningMessage", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *continueAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NDSDatabaseParser *parser = [[NDSDatabaseParser alloc] initWithDelegate:self];
        
        self.progressHUD = [[M13ProgressHUD alloc] initWithProgressView:[[M13ProgressViewRing alloc] init]];
        self.progressHUD.progressViewSize = CGSizeMake(100.0, 100.0);
        self.progressHUD.animationPoint = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 3);
        
        [self.view addSubview:self.progressHUD];
        [self.progressHUD show:YES];
        self.progressHUD.status = @"Downloading Database";
        
        
        [parser updateDatabase];
        [userDefaults setObject:[NSDate date] forKey:kUserDefaultsLastDatabaseUpdate];
        [userDefaults synchronize];
        
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:continueAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)databaseParser:(NDSDatabaseParser *)parser didFinishParsing:(NSError *)error {
    [self.progressHUD dismiss:YES];
}

-(void)databaseParser:(NDSDatabaseParser *)parser didReportProgress:(NSProgress *)progress {
    if (![self.progressHUD.status isEqualToString:progress.userInfo[kProgressMessageString]]) {
        self.progressHUD.status = progress.userInfo[kProgressMessageString];
    }
    
    [self.progressHUD setProgress:progress.fractionCompleted animated:NO];
    
    if (progress.fractionCompleted == 1) {
        [self.progressHUD dismiss:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *lastUpdateDate = [userDefaults objectForKey:kUserDefaultsLastDatabaseUpdate];
    
    if (lastUpdateDate == nil) {
        self.lastDatabaseRefreshDateLabel.text = @"Never";
    } else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        dateFormatter.dateFormat = kDateFormatString;
        self.lastDatabaseRefreshDateLabel.text = [dateFormatter stringFromDate:lastUpdateDate];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
