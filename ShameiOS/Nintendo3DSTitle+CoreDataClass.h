//
//  Nintendo3DSTitle+CoreDataClass.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Nintendo3DSTitle : NSManagedObject

+(NSString *)name;

@end

NS_ASSUME_NONNULL_END

#import "Nintendo3DSTitle+CoreDataProperties.h"
