//
//  Nintendo3DSTitle+CoreDataClass.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "Nintendo3DSTitle+CoreDataClass.h"

@implementation Nintendo3DSTitle

+(NSString *)name {
    return @"Nintendo3DSTitle";
}

-(NSString *)scrollerLetter {
    
    NSString *firstLetter = [[self.name substringToIndex:1] uppercaseString];
    NSCharacterSet *lettersCharacterSet = [NSCharacterSet uppercaseLetterCharacterSet];
    NSCharacterSet *numbersCharacterSet = [NSCharacterSet decimalDigitCharacterSet];
    
    
    if ([firstLetter rangeOfCharacterFromSet:lettersCharacterSet].location != NSNotFound) {
        return firstLetter;
    } else if ([firstLetter rangeOfCharacterFromSet:numbersCharacterSet].location != NSNotFound) {
        return @"#";
    } else {
        return @"?";
    }
}

-(int)sortOrder {
    NSString *firstLetter = self.scrollerLetter;
    NSCharacterSet *lettersCharacterSet = [NSCharacterSet uppercaseLetterCharacterSet];
    
    if ([firstLetter isEqualToString:@"#"]) {
        return 0;
    } else if ([firstLetter rangeOfCharacterFromSet:lettersCharacterSet].location != NSNotFound) {
        return 1;
    } else if ([firstLetter isEqualToString:@"?"]) {
        return 2;
    } else {
        return 3;
    }
}

@end
