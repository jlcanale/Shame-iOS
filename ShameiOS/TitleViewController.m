//
//  TitleViewController.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/16/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "TitleViewController.h"
#import "AppDelegate.h"

@interface TitleViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleRegionLabel;
@property (weak, nonatomic) IBOutlet UILabel *serialNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *encTitleKeyLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *QRCodeImageView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *favoriteBarButtonItem;


@end

@implementation TitleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titleNameLabel.text = self.ndsTitle.name;
    self.titleRegionLabel.text = self.ndsTitle.region;
    self.serialNumberLabel.text = self.ndsTitle.serial;
    self.titleIDLabel.text = self.ndsTitle.titleID;
    self.encTitleKeyLabel.text = self.ndsTitle.encTitleKey;
    self.typeLabel.text = self.ndsTitle.type;
    self.sizeLabel.text = [NSByteCountFormatter stringFromByteCount:self.ndsTitle.size countStyle:NSByteCountFormatterCountStyleDecimal];
    
    if (self.ndsTitle.isFavorite) {
        self.favoriteBarButtonItem.image = [UIImage imageNamed:@"star"];
    }
    
    
    NSData *QRData = [[NSString stringWithFormat:@"https://3ds.titlekeys.com/ticket/%@", self.ndsTitle.titleID] dataUsingEncoding:NSISOLatin1StringEncoding allowLossyConversion:NO];
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setValue:QRData forKey:@"inputMessage"];
    [filter setValue:@"Q" forKey:@"inputCorrectionLevel"];
    
    float scaleX = self.QRCodeImageView.frame.size.width / [filter.outputImage extent].size.width;
    float scaleY = self.QRCodeImageView.frame.size.height / [filter.outputImage extent].size.height;
    
    
    self.QRCodeImageView.image = [UIImage imageWithCIImage:[filter.outputImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)favoriteTouched:(UIBarButtonItem *)sender {
    self.ndsTitle.isFavorite = !self.ndsTitle.isFavorite;
    
    if (self.ndsTitle.isFavorite) {
        self.favoriteBarButtonItem.image = [UIImage imageNamed:@"star"];
    } else {
        self.favoriteBarButtonItem.image = [UIImage imageNamed:@"emptyStar"];
    }
    
    NSError *error;
    
    [((AppDelegate *)[UIApplication sharedApplication].delegate).persistentContainer.viewContext save:&error];
    
    if (error) {
        NSLog(@"Error saving favorite :%@", error);
    }
}


@end
