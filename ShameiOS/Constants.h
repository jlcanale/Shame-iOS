//
//  Constants.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/16/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define kUserDefaultsFiltersKey @"selectedFilterIndexPaths"
#define kUserDefaultsUnknownsKey @"unknownTitlesFilter"
#define kUserDefaultsLastDatabaseUpdate @"lastDatabaseUpdateDate"

#define kDateFormatString @"M/d/YYYY hh:MM a"

#define kProgressMessageString @"progressMessage"

#endif /* Constants_h */
