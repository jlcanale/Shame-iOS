//
//  TitlesTableViewController.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/15/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "TitlesTableViewController.h"
#import "NDSDatabaseParser.h"
#import "AppDelegate.h"
#import "Nintendo3DSTitle+CoreDataClass.h"
#import "TitleViewController.h"
#import "Constants.h"
#import <M13ProgressSuite/M13ProgressViewRing.h>
#import <M13ProgressSuite/M13ProgressHUD.h>


@interface TitlesTableViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) M13ProgressHUD *progressHUD;

@end

@implementation TitlesTableViewController

@synthesize persistentContainer = _persistentContainer;



- (NSPersistentContainer *)persistentContainer {
    if (!_persistentContainer) {
        _persistentContainer = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer;
    }
    return _persistentContainer;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSError *error = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:[Nintendo3DSTitle name] inManagedObjectContext:self.persistentContainer.viewContext];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
    NSSortDescriptor *secondarySortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"scrollerLetter" ascending:YES];
    NSSortDescriptor *thirdSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    fetchRequest.sortDescriptors = @[sortDescriptor, secondarySortDescriptor, thirdSortDescriptor];

    fetchRequest.entity = entity;
    fetchRequest.fetchBatchSize = 20;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *namePredicateArray = [[NSMutableArray alloc] init];
    NSMutableArray *regionPredicateArray = [[NSMutableArray alloc] init];
    NSMutableArray *typePredicateArray = [[NSMutableArray alloc] init];
    NSMutableArray *fetchPredicateArray = [[NSMutableArray alloc] init];
    
    //Filter Names
    if (![userDefaults boolForKey:kUserDefaultsUnknownsKey]) {
        [namePredicateArray addObject:[NSPredicate predicateWithFormat:@"name != 'Unknown'"]];
    }
    if (self.searchBar.text.length) {
        [namePredicateArray addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", self.searchBar.text]];
    }
    if (namePredicateArray.count != 0) {
        [fetchPredicateArray addObject:[NSCompoundPredicate andPredicateWithSubpredicates:namePredicateArray]];
    }
    
    //Filter Regions & Types
    NSMutableArray *indexPathDataArray = [userDefaults objectForKey:kUserDefaultsFiltersKey];
    
    for (NSData *indexPathData in indexPathDataArray) {
        
        NSIndexPath *indexPath = [NSKeyedUnarchiver unarchiveObjectWithData:indexPathData];
        //Filter Regions
        if (indexPath.section == 0) {
            switch (indexPath.row) {
                case 0:
                    //Filter for All
                    [regionPredicateArray addObject:[NSPredicate predicateWithFormat:@"region IN {'USA', 'EUR', 'JPN'}"]];
                    break;
                case 1:
                    //Filter for USA
                    [regionPredicateArray addObject:[NSPredicate predicateWithFormat:@"region == 'USA'"]];
                    break;
                case 2:
                    //Filter for EUR
                    [regionPredicateArray addObject:[NSPredicate predicateWithFormat:@"region == 'EUR'"]];
                    break;
                case 3:
                    //Filter for JPN
                    [regionPredicateArray addObject:[NSPredicate predicateWithFormat:@"region == 'JPN'"]];
                    break;
                default:
                    break;
            }
        }
        
        //Filter Types
        if (indexPath.section == 1) {
            switch (indexPath.row) {
                case 0:
                    //Filter for All
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type IN {'eShop', 'System Application', 'System Data Archive', 'System Applet', 'System Module', 'System Firmware', 'Download Play Title','DSIWare System Application','DSIWare System Data Archive','DSIWare','Update','Demo','DLC'}"]];
                    break;
                case 1:
                    //Filter for eShop
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type == 'eShop'"]];
                    break;
                case 2:
                    //Filter for System
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type IN {'System Application', 'System Data Archive', 'System Applet', 'System Module', 'System Firmware'}"]];
                    break;
                case 3:
                    //Filter for Demo
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type == 'Demo'"]];
                    break;
                case 4:
                    //Filter for DLC
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type == 'DLC'"]];
                    break;
                case 5:
                    //Filter for Update
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type == 'Update'"]];
                    break;
                case 6:
                    //Filter for DSIWare
                    [typePredicateArray addObject:[NSPredicate predicateWithFormat:@"type IN {'DSIWare System Application','DSIWare System Data Archive','DSIWare'}"]];
                    break;
                default:
                    break;
            }
        }
    }
    
    if (regionPredicateArray.count != 0) {
        [fetchPredicateArray addObject:[NSCompoundPredicate orPredicateWithSubpredicates:regionPredicateArray]];
    }
    
    if (typePredicateArray.count != 0) {
        [fetchPredicateArray addObject:[NSCompoundPredicate orPredicateWithSubpredicates:typePredicateArray]];
    }
    
    
    fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:fetchPredicateArray];
    
    NSFetchedResultsController *resultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.persistentContainer.viewContext sectionNameKeyPath:nil cacheName:nil];
    _fetchedResultsController = resultsController;
    _fetchedResultsController.delegate = self;
    
    [self.fetchedResultsController performFetch:&error];
    
    if (error != nil){
        NSLog(@"%@",error);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.tableView reloadData];
    });
    
    return _fetchedResultsController;
}

- (void)viewDidAppear:(BOOL)animated {
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *lastDatabaseUpdateDate = [userDefaults objectForKey:kUserDefaultsLastDatabaseUpdate];
    
    if (lastDatabaseUpdateDate == nil) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"WarningMessage", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //abort();
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.fetchedResultsController performFetch:nil];
            });
            
        }];
        
        UIAlertAction *continueAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NDSDatabaseParser *parser = [[NDSDatabaseParser alloc] initWithDelegate:self];

            self.progressHUD = [[M13ProgressHUD alloc] initWithProgressView:[[M13ProgressViewRing alloc] init]];
            self.progressHUD.progressViewSize = CGSizeMake(60.0, 60.0);
            self.progressHUD.animationPoint = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2);
            
            [self.view addSubview:self.progressHUD];
            [self.progressHUD show:YES];
            self.progressHUD.status = @"Downloading Database";
            
            
            [parser updateDatabase];
            [userDefaults setObject:[NSDate date] forKey:kUserDefaultsLastDatabaseUpdate];
            [userDefaults synchronize];
            
            
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:continueAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
}

- (void)databaseParser:(NDSDatabaseParser *)parser didFinishParsing:(NSError *)error {
    [self.progressHUD dismiss:YES];
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

-(void)databaseParser:(NDSDatabaseParser *)parser didReportProgress:(NSProgress *)progress {
    if (![self.progressHUD.status isEqualToString:progress.userInfo[kProgressMessageString]]) {
        self.progressHUD.status = progress.userInfo[kProgressMessageString];
    }
    
    [self.progressHUD setProgress:progress.fractionCompleted animated:NO];
    
    if (progress.fractionCompleted == 1) {
        [self.progressHUD dismiss:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fetchedResultsController.sections[section] numberOfObjects];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell" forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Nintendo3DSTitle *title = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = title.name;
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"First Letter: %@, Sort Group %i", title.scrollerLetter, title.sortOrder];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Title ID: %@", title.titleID];
}


 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showTitleSegue"]) {
        TitleViewController *destinationViewController = (TitleViewController*)segue.destinationViewController;
        destinationViewController.ndsTitle = [self.fetchedResultsController objectAtIndexPath:self.tableView.indexPathForSelectedRow];
    } else if ([segue.identifier isEqualToString:@"filtersSegue"]) {
        self.tableView.contentOffset  = CGPointZero;
    }
}

#pragma mark NSFetchedResultsController Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            break;
        case NSFetchedResultsChangeMove:
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.fetchedResultsController.sectionIndexTitles;
}

#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar endEditing:YES];
}

@end
