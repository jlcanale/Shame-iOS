//
//  FavoritesTableViewController.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/17/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Nintendo3DSTitle+CoreDataClass.h"

@interface FavoritesTableViewController : UITableViewController

@property (readonly) NSPersistentContainer *persistentContainer;

@end
