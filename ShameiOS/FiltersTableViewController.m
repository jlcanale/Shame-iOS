//
//  FiltersTableViewController.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/16/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "FiltersTableViewController.h"
#import "Constants.h"

@interface FiltersTableViewController ()

@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;
@property (weak, nonatomic) IBOutlet UISwitch *unknownTitlesSwitch;

@end

@implementation FiltersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *indexPathDataArray = [userDefaults objectForKey:kUserDefaultsFiltersKey];
    
    self.unknownTitlesSwitch.on = [userDefaults boolForKey:kUserDefaultsUnknownsKey];
    
    self.selectedIndexPaths = [NSMutableArray arrayWithCapacity:indexPathDataArray.count];
    
    for (NSData *indexPathData in indexPathDataArray) {
        
        NSIndexPath *indexPath = [NSKeyedUnarchiver unarchiveObjectWithData:indexPathData];
        
        [self.selectedIndexPaths addObject:indexPath];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)applyButtonPressed:(UIBarButtonItem *)sender {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *encodedIndexPaths = [NSMutableArray arrayWithCapacity:self.selectedIndexPaths.count];
    
    for (NSIndexPath *indexPath in self.selectedIndexPaths) {
        [encodedIndexPaths addObject:[NSKeyedArchiver archivedDataWithRootObject:indexPath]];
    }
    
    [userDefaults setObject:encodedIndexPaths forKey:kUserDefaultsFiltersKey];
    [userDefaults setBool:self.unknownTitlesSwitch.on forKey:kUserDefaultsUnknownsKey];
    
    [userDefaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.selectedIndexPaths containsObject:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row == 0 && (indexPath.section == 0 || indexPath.section == 1)) {
        NSInteger rowCount = [tableView numberOfRowsInSection:indexPath.section];
        
        for (int i = 1; i < rowCount; i++) {
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
            [self clearAccessoryViewForRowAtIndexPath:currentIndexPath];
            [self.selectedIndexPaths removeObject:currentIndexPath];
        }
    } else if (indexPath.row != 0 && (indexPath.section == 0 || indexPath.section == 1)) {
        NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
        [self clearAccessoryViewForRowAtIndexPath:currentIndexPath];
        [self.selectedIndexPaths removeObject:currentIndexPath];
    }
    
    if (![self.selectedIndexPaths containsObject:indexPath]) {
        [self.selectedIndexPaths addObject:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        [self.selectedIndexPaths removeObject:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)clearAccessoryViewForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}

-(void)setAccessoryViewForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
