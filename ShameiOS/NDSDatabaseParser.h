//
//  DatabaseParser.h
//  ShameiOS
//
//  Created by Joseph Canale on 3/15/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NDSDatabaseParser;

@protocol NDSDatabaseParserDelegate <NSObject>

@required

@property (readonly) NSPersistentContainer *persistentContainer;

@optional

- (void)databaseParser:(NDSDatabaseParser *)parser didFinishParsing:(NSError *)error;

- (void)databaseParser:(NDSDatabaseParser *)parser didReportProgress:(NSProgress *)progress;

@end

@interface NDSDatabaseParser : NSObject

@property (nonatomic, strong) id<NDSDatabaseParserDelegate> delegate;

- (instancetype)initWithDelegate:(id<NDSDatabaseParserDelegate>)delegate;

- (void)updateDatabase;

@end



