//
//  DatabaseParser.m
//  ShameiOS
//
//  Created by Joseph Canale on 3/15/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import "NDSDatabaseParser.h"
#import "Nintendo3DSTitle+CoreDataClass.h"
#import "Constants.h"


@interface NDSDatabaseParser()

@property (readonly) NSURL *titleDatabaseURL;
@property (readonly) NSURL *sizeDatabaseURL;
@property (readonly) NSDictionary *typeDictionary;
@property (nonatomic, strong) NSProgress *progress;

@end

@implementation NDSDatabaseParser

-(instancetype)initWithDelegate:(id<NDSDatabaseParserDelegate>)delegate {
    if (self = [super init]) {
        self.delegate = delegate;
    }
    
    return self;
}

- (void)updateDatabase {
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    self.progress = [NSProgress progressWithTotalUnitCount:1];
    [self.progress setUserInfoObject:@"Downloading Database" forKey:kProgressMessageString];
    [self reportProgress];
    
    NSURLSessionDataTask *task = [defaultSession dataTaskWithURL:self.titleDatabaseURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            [self loadDatabaseFromJSON:data];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(databaseParser:didFinishParsing:)]) {
                [self.delegate databaseParser:self didFinishParsing:error];
            }
        });
    }];
    
    [task resume];
    
}


- (void)loadDatabaseFromJSON:(NSData *)json {
    NSError *error = nil;
    NSArray *titles = [NSJSONSerialization JSONObjectWithData:json options:0 error:&error];
    
    self.progress = [NSProgress discreteProgressWithTotalUnitCount:titles.count];
    [self.progress setUserInfoObject:@"Parsing Files" forKey:kProgressMessageString];

    NSManagedObjectContext *backgroundContext = self.delegate.persistentContainer.newBackgroundContext;
    
    for (NSDictionary *titleDictionary in titles) {
        NSEntityDescription *entity = [NSEntityDescription entityForName:[Nintendo3DSTitle name] inManagedObjectContext:self.delegate.persistentContainer.viewContext];
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"titleID == %@", titleDictionary[@"titleID"]];
        
        NSArray *results = [backgroundContext executeFetchRequest:fetchRequest error:&error];
        
        Nintendo3DSTitle *title = nil;
        
        if (results.count > 0)
        {
             title = results[0];

        } else {
             title = [[Nintendo3DSTitle alloc] initWithEntity:entity insertIntoManagedObjectContext:backgroundContext];
             title.titleID = titleDictionary[@"titleID"];
        }
        
        title.name = [titleDictionary[@"name"] isEqualToString: @""] ? @"Unknown" : [titleDictionary[@"name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        title.encTitleKey = titleDictionary[@"encTitleKey"] == nil ? @"" : titleDictionary[@"encTitleKey"];
        title.region = titleDictionary[@"region"] == nil ? @"Unknown" : titleDictionary[@"region"];
        title.serial = [titleDictionary[@"serial"] isKindOfClass:[NSNull class]] ? @"Unknown" : titleDictionary[@"serial"];
        title.size = (int)titleDictionary[@"size"];
        
        title.type = self.typeDictionary[[title.titleID substringToIndex:8]];
        
        self.progress.completedUnitCount = self.progress.completedUnitCount + 1;
        [self reportProgress];
        
    }
    
    [backgroundContext save:&error];
    
    
}

- (void)reportProgress {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(databaseParser:didReportProgress:)]) {
            [self.delegate databaseParser:self didReportProgress:self.progress];
        }
    });
    
}

- (NSURL *)titleDatabaseURL {
    return [NSURL URLWithString:@"https://3ds.titlekeys.com/json_enc"];
}

- (NSURL *)sizeDatabaseURL {
    return [NSURL URLWithString:@"http://housebreaker.net/sizes.json"];
}

-(NSDictionary *)typeDictionary {
    return @{@"00040000": @"eShop",
             @"00040010": @"System Application",
             @"0004001B": @"System Data Archive",
             @"000400DB": @"System Data Archive",
             @"0004009B": @"System Data Archive",
             @"00040030": @"System Applet",
             @"00040130": @"System Module",
             @"00040138": @"System Firmware",
             @"00040001": @"Download Play Title",
             @"00048005": @"DSIWare System Application",
             @"0004800F": @"DSIWare System Data Archive",
             @"00048000": @"DSIWare",
             @"00048004": @"DSIWare",
             @"0004000E": @"Update",
             @"00040002": @"Demo",
             @"0004008C": @"DLC"
             };
}


@end
